const express = require("express");
const app = express();
const http = require("http");
const server = http.Server(app);
const PORT = process.env.PORT || 8080;
const cors = require("cors");
const connect = require("./DB/connectDB");
require("dotenv").config();
app.use(cors());

// static files
app.use(express.static("uploads"));
app.get("/", (req, res) => {
  res.send("Hello world");
});

// impoerting routes
const userRoutes = require("./routes/userRoutes");
const meetRoutes = require("./routes/meetroutes");

// middlewares
app.use(express.json());

// routes
app.use("/api/users", userRoutes);
app.use("/api/meet", meetRoutes);

const start = async () => {
  await connect(process.env.DB_CONNEXIONCLOUD).then(() => {
    console.log("Connected to database");
  });
  server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
};
start();
