const mongoose = require("mongoose");
mongoose.set("strictQuery", true);
const connect = (code) => {
  return mongoose.connect(code);
};
module.exports = connect;
