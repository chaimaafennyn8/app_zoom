const mongoose = require("mongoose");
const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: "MEET N : ",
    lowercase: true,
  },
  owner: {
    type: mongoose.SchemaTypes.ObjectId,
    required: false,
  },
  creationTime: {
    type: Date,
    default: Date.now,
  },
  startTime: {
    type: Date,
    required: false,
  },
  endTime: {
    type: Date,
    required: false,
  },
  private: {
    type: Boolean,
    default: false,
  },
  participants: {
    type: [String],
    default: [],
  },
  description: {
    type: String,
    default: "",
  },
});
const meetingModel = mongoose.model("meet", schema);
module.exports = meetingModel;
