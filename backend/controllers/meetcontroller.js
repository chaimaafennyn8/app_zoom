const { verifyToken } = require("../utils/jwt");
const meetModel = require("../models/meetmodel");

const createMeet = async (req, res) => {
  const test = verifyToken(req.headers.authorization.split(" ")[1]);
  console.log(test);
  if (!test) {
    res.status(404).send("invalid token");
  }
  const meet = req.body;
  try {
    console.log(meet);
    const newMeet = await meetModel.create({ ...meet, owner: test.id });
    res.status(200).json({ newMeet });
  } catch (error) {
    res.status(400).send(error);
  }
};

const getAll = async (req, res) => {
  const test = verifyToken(req.headers.authorization.split(" ")[1]);
  console.log(test);
  if (!test) {
    res.status(404).send("invalid token");
  }
  try {
    const result = await meetModel.find({ owner: test.id });

    res.status(200).send(result);
  } catch (error) {
    res.status(400).send(error);
  }
};

const deleteOne = async (req, res) => {
  const test = verifyToken(req.headers.authorization.split(" ")[1]);
  console.log(test);
  if (!test) {
    res.status(404).send("invalid token");
  }
  try {
    await meetModel.findByIdAndDelete(req.body.id);
    res.status(200).send("deleted");
  } catch (error) {
    res.status(400).send(error);
  }
};
const updateOne = async (req, res) => {
  const test = verifyToken(req.headers.authorization.split(" ")[1]);
  console.log(test);
  if (!test) {
    res.status(404).send("invalid token");
  }
  try {
    const meet = await meetModel.findByIdAndUpdate(req.body._id, req.body, {
      new: true,
    });
    res.status(200).json({ meet });
  } catch (error) {
    res.status(400).send(error);
  }
};
module.exports = { createMeet, getAll, deleteOne, updateOne };
